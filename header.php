<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
  <head>

    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="theme-color" content="#09afa7">
    <title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <meta name="description" content="<?php bloginfo('description'); ?>">
    <meta charset="utf-8">

    <link href="https://fonts.googleapis.com/css?family=Titillium+Web&display=swap" rel="stylesheet">

    <?php 
      $image = get_template_directory() . "/screenshot.jpg";
      $image_url = get_template_directory_uri() . "/screenshot.jpg";
      list($width_screen, $height_screen) = getimagesize($image);

      $url = get_bloginfo('url');
      $title = get_bloginfo('name');

      if(is_single() || is_page()) {
        $url = get_the_permalink();
        $title = get_the_title();
      }
    ?>

    <meta property="og:url" content="<?php echo $url; ?>"/>
    <meta property="og:title" content="<?php echo $title; ?>" />
    <meta property="og:description" content="<?php bloginfo('description'); ?>" />
    <meta property="og:image" content="<?php echo $image_url; ?>" />
    <meta property="og:image:type" content="image/jpg"/>
    <meta property="og:image:width" content="<?php echo $width_screen; ?>"/>
    <meta property="og:image:height" content="<?php echo $height_screen; ?>"/>
    <meta property="og:type" content="article" />
    <meta property="fb:app_id" content="665532330559584">


    <?php wp_head(); ?>

  </head>
  <body <?php body_class(); ?>>