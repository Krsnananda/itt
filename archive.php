<?php get_header(); ?>
<header class="header clear">
  <?php get_template_part('components/item', 'head-menu'); ?>
</header>
<main role="main">
<?php

  $customLists = array('news', 'placar');
  if (in_array(get_post_type(), $customLists)) {
      get_template_part('lists/list-' . get_post_type());
  } else {
      get_template_part('lists/list-default');
  }
  
  //get_template_part('components/item','pagination'); 
?>
</main>

<?php get_footer(); ?>