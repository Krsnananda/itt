<?php
$query_args = array(
  'post_type' => 'news',
  'post_status' => 'publish',
  'category_news' => 'schedule',
  'orderby' => 'date-schedule',
  'order'   => 'ASC',
  'posts_per_page' => 3
);
$the_query = new WP_Query($query_args);
$schedules = array();

while ($the_query->have_posts()){
  $the_query->the_post();
  //$date = substr(get_field('date-schedule'), 0, 5);
  $schedules[] = array(
    'title' => get_the_title(),
    'date' => get_field('date-schedule'),
    'link' => get_post_permalink()
  );
}
?>
<?php if ($the_query->have_posts()) : ?>

<div class="mt-4">
  <h4 class="text-right">
    <?php _e('AGENDA', 'morrinhos'); ?>
  </h4>
  <div class="schedule panel">
    <?php foreach ($schedules as $schedule):?>
      <a href="<?php echo $schedule['link'];?>">
        <div class="row mb-3">
          <div class="col-4 calendar-date-col">
            <div class="calendar-date">
              <div class="day">
              <?php 
                $date = strtotime(Util::datePtToEn($schedule['date']));
                echo strftime("%d", $date);
              ?>
              </div>
              <div class="month">
                <?php echo substr(strtoupper(__(date('F',$date), 'morrinhos')), 0, 3); ?>
              </div>
            </div>
          </div>
          <div class="col">
            <p class="schedule-title"><?php echo $schedule['title'];?></p>
          </div>
        </div>
      </a>
    <?php endforeach;?>
    <a href="/category_news/schedule/" class="btn btn-primary w-100" role="button">
      <?php echo _e('VEJA MAIS', 'morrinhos'); ?>
    </a>
  </div>
</div>
<?php endif ?>