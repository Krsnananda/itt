<?php

  $showAll = $_GET['showAll'];
  $posts_per_page = 6;

  $query_args = array(
    'post_type' => 'news',
    'post_status' => 'publish',
    'posts_per_page' => $showAll ? -1 : $posts_per_page
  );

  $the_query = new WP_Query($query_args);
  $news = array();

  while ($the_query->have_posts()){
    $the_query->the_post();
    $news[] = array(
      'id' => $post->ID,
      'title' => get_the_title(),
      'description' => get_field('description'),
      'date-schedule' => get_field('date-schedule'),
      'date' => get_the_date('Y-m-d'),
      'link' => get_post_permalink(),
      'img' => get_the_post_thumbnail_url(),
    );
  }

  if ($is_schedule) {
    function sortByDate($a, $b) {
      return strtotime(Util::datePtToEn($b['date-schedule'])) - strtotime(Util::datePtToEn($a['date-schedule']));
    }
    usort($news, 'sortByDate');
  }

?>
<section>
  <div class="row">
    <div class="col">
      <h4>
        <?php _e('NOTÍCIAS', 'morrinhos'); ?>
      </h4>
    </div>
    <?php if (count($news) > 0): foreach ($news as $key => $row):?>
      <?php if ($key == $posts_per_page): ?>
        <div id="showmore"></div>
      <?php endif;?>
      <div class="col-sm-12 mt-4">
        <?php 
          set_query_var('news',$row);
          get_template_part('components/item','card-news');
        ?>
      </div> 
      <?php endforeach; ?>
      <?php if (!$showAll && count($news) >= $posts_per_page): ?>
        <div class="col-sm-12 my-4">
          <a class="btn btn-primary w-100" href="?showAll=true#showmore">
            <?php _e('VEJA MAIS', 'morrinhos'); ?>
          </a>
        </div>
      <?php endif ?>
    <?php endif ?>
  </div>
</section>
