<?php
$footerLeft = Util::getWidgetObj('footer-left');
$footerRight = Util::getWidgetObj('footer-right');

?>

<footer class="footer" role="contentinfo">
  <div class="container pb-5"> 
    <hr>
    <!-- Mapsite -->
    <nav class="row mb-2">
      <?php custom_menu_footer('footer-menu'); ?>
    </nav>
    <div class="row">
      <div class="col-md">
        <div class="row">
          <div class="col-md-3">
            <a href="/">
              <img src="<?php echo images_path() . '/brasao.png'; ?>">
            </a>
          </div>
          <div class="col-md pt-3">
            <strong>
              <?php echo $footerLeft['items'][0]['content'] ?>
            </strong>
            <div>
              <?php echo $footerLeft['items'][1]['content'] ?>
            </div>
            <div>
              <?php echo $footerLeft['items'][2]['content'] ?>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md text-right mt-md-0 mt-3">
        <h4>
          <strong>
            <img class="icon" src="<?php echo images_path() . '/telefone.svg'; ?>">
            <?php echo $footerRight['items'][0]['content'] ?>
          </strong>
        </h4>
        <div>
          <?php echo $footerRight['items'][1]['content'] ?>
        </div>
        <div>
          <?php echo $footerRight['items'][2]['content'] ?>
        </div>
      </div>
    </div>
  </div>
  <div class="copyright small py-4">
    <div class="container">
      <div class="row">
        <div class="col-md-3 mb-md-0 mb-3">
          &copy;<?php echo _e('Copyright', 'morrinhos') .' '. date("Y"); ?>
        </div>
        <div class="col-md-6 mb-md-0 mb-3 text-center">
          <?php echo _e('Desenvolvido por', 'morrinhos'); ?>
          <a href="https://www.eagledigital.com.br/" target="_blank">
            <img src="<?php echo images_path() . '/logo-eagle-branco.svg'; ?>" alt="Eagle Inteligência Digital" title="Eagle Inteligência Digital"/>
          </a>
        </div>
        <div class="col-md-3 mb-md-0 mb-3 text-right">
          <?php echo _e('Todos os direitos reservados', 'morrinhos'); ?>
        </div>
      </div>
    </div>
  </div>
</footer>
<div class="colors-bar">
  <div class="color-bar1"></div>
  <div class="color-bar2"></div>
  <div class="color-bar3"></div>
  <div class="color-bar4"></div>
  <div class="color-bar5"></div>
  <div class="color-bar6"></div>
</div>
