<?php

$query_args = array(
  'post_type' => 'placar',
  'post_status' => 'publish',
  'orderby' => 'date',
  'order'   => 'ASC',
  'posts_per_page' => 3
);
$the_query = new WP_Query($query_args);
$placares = array();

while ($the_query->have_posts()){
  $the_query->the_post();
  $placares[] = array(
    'title' => get_the_title(),
    'time1' => get_field('time1'),
    'pontos1' => get_field('pontos1'),
    'time2' => get_field('time2'),
    'pontos2' => get_field('pontos2'),
    'date' => get_field('date'),
    'link' => get_post_permalink()
  );
}
function sortByDate($a, $b) {
  return strtotime(Util::datePtToEn($b['date'])) - strtotime(Util::datePtToEn($a['date']));
}
usort($placares, 'sortByDate');

?>
<?php if ($the_query->have_posts()) : ?>

<div class="mt-4">
  <h4 class="text-right">
    <?php _e('CAMPEONATO MUNICIPAL', 'morrinhos'); ?>
  </h4>
  
  <div class="panel placar">
    <?php 
      set_query_var('placares', $placares);
      get_template_part('components/item', 'panel-placar');
    ?>
    <a href="/placar/" class="btn btn-primary w-100" role="button">
      <?php echo _e('VEJA MAIS', 'morrinhos'); ?>
    </a>
  </div>
</div>

<?php endif ?>