<?php
  $background = get_query_var('background');    

  if (get_post_type() == 'page' && get_the_post_thumbnail_url()) {
    $background = get_the_post_thumbnail_url();
  }

  $background = array_shift(Util::getWidgetImgPath('header-default-background'));

  $background = $background ? $background : images_path() . '/banner1.jpg';

  $title = get_query_var('title') ?
    get_query_var('title') :
    get_the_title();
?>
<div class="head-page" 
  style="background-image: url(<?php echo $background; ?>);">
  <div class="head-title">
    <h2 class="p-3">
      <?php echo $title; ?>
    </h2>
  </div>
</div>
<?php if(get_query_var('subtitle')): ?>
<div class="category-title w-100 p-3 mt-4">
  <div class="container">
    <h5 class="my-2">
      <?php echo get_query_var('subtitle'); ?>
    </h5>
  </div>
</div>
<?php endif; ?>