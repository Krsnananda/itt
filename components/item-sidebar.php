<?php get_template_part('components/item', 'share'); ?>
<div class="mt-4">

  <?php custom_menu_sidebar('links-uteis-menu'); ?>  

</div>

<div class="mt-4">
  <h4 class="text-right">
    <?php _e('ACESSO RÁPIDO', 'morrinhos'); ?>
  </h4>
  <?php custom_menu_sidebar('sidebar-menu', true); ?>
</div>
<div class="mt-4">
  <h4 class="text-right">
    <?php _e('ACESSO RESTRITO', 'morrinhos'); ?>
  </h4>
  <?php custom_menu_sidebar('restrito-menu', true); ?>
</div>