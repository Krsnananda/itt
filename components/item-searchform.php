<div class="search-form py-3">
  <div class="container">
    <form class="form-inline h-100" id="search-form" method="get" action="/" role="search">
      <input class="form-control mr-2" type="search" placeholder="O que você procura?" name="s" id="s" aria-label="Search" value="<?php echo get_search_query(); ?>">
      <button type="submit" id="btn-search" >
        <img src="<?php echo images_path() . '/pesquisa.svg'; ?>">
      </button>
    </form>
  </div>
</div>
