<!-- Carousel news -->
<?php
$query_args = array(
  'post_type' => 'news',
  'category_news' => 'banner',
  'post_status' => 'publish',
  'posts_per_page' => 3
);
$the_query = new WP_Query($query_args);

$news = array();

while ($the_query->have_posts()){
  $the_query->the_post();
  $news[] = array(
    'id' => $post->ID,
    'title' => get_the_title(),
    'description' => get_field('description'),
    'date-schedule' => get_field('date-schedule'),
    'date' => get_the_date('Y-m-d'),
    'link' => get_post_permalink(),
    'img' => get_the_post_thumbnail_url(),
  );
}


?>
<?php if (count($news) > 0): ?>
<div id="carousel" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <?php foreach ($news as $key => $row): ?>
      <li data-target="#carousel" data-slide-to="<?php echo $key ?>" <?php echo !$key ? 'class="active"' : ''; ?>></li>
    <?php endforeach; ?>
  </ol>
  <div class="carousel-inner h-100 w-100">
    <?php foreach ($news as $key => $row): ?>
      <div class="carousel-item <?php echo !$key ? 'active' : ''; ?> h-100 w-100"
        style="background-image: url('<?php echo $row['img']; ?>')">
        <div class="carousel-caption d-none d-md-block">
          <div class="mb-1">
            <?php 
            $categories = get_the_terms($row['id'], 'category_news');
            $categories = $categories ? $categories : array();
            if ($categories):
              foreach($categories as $category): 
                if ($category->slug != 'banner'): ?>
                <span class="badge badge-primary">
                  <?php echo  $category->name ?>
                </span>
              <?php endif; endforeach; endif; ?>
          </div>
          <h4><?php echo $row['title']; ?></h4>
        </div>
      </div>
    <?php endforeach; ?>
  </div>
  <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<?php endif ?>
