<?php 
$placares = get_query_var('placares');
if (!$placares) {
  return;
}
?>
<?php foreach ($placares as $placar):?>
  <div class="row m-1">
    <div class="col small text-center">
      <?php 
        $date = strtotime(Util::datePtToEn($placar['date']));
        echo substr(strtoupper(__(date('l',$date), 'morrinhos')), 0, 3);
        echo ' '.date('d/m/Y',$date);
        echo ' - '.date('H:i',$date);
      ?>
    </div>
  </div>
  <div class="row resultado mb-3 pb-2 mx-1">
    <div class="col time text-left">
      <?php echo $placar['time1'];?>
    </div>
    <div class="col text-center">
      <span class="pontos"><?php echo $placar['pontos1'];?></span>x<span class="pontos"><?php echo $placar['pontos2'];?></span>
    </div>
    <div class="col time text-right">
      <?php echo $placar['time2'];?>
    </div>
  </div>
<?php endforeach;?>
