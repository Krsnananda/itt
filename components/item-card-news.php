<?php 
$news = get_query_var('news');
if (!$news) {
  return;
}
?>
<div class="card card-horizontal panel">
  <div class="row">
    <div class="col-md-4 card-section-left">
      <?php if ($news['date-schedule']): ?>
        <div class="w-100 h-100 d-flex">
          <div class="schedule-date m-auto">
            <?php
              $dateSchedule = strtotime(Util::datePtToEn($news['date-schedule']));
              echo date('d/m', $dateSchedule);
            ?>
          </div>
        </div>
      <?php else: ?>
        <img src="<?php echo $news['img']; ?>">
      <?php endif; ?>
    </div>
    <div class="col-md-8 card-section-right">
      <div class="card-body">
        <div class="mb-1">
          <?php 
          $categories = get_the_terms($news['id'], 'category_news');
          $categories = $categories ? $categories : array();
          if ($categories):
            foreach($categories as $category): ?>
              <span class="badge badge-primary">
                <?php echo  $category->name ?>
              </span>
            <?php endforeach; endif; ?>
        </div>
        <a href="<?php echo $news['link'];?>">
          <h5 class="card-title mb-2">
            <?php echo $news['title']; ?>
          </h5>
        </a>
        <p class="card-text">
          <?php echo $news['description']; ?>
        </p>
      </div>
      <div class="card-footer">
        <div class="small d-inline">
          <?php
          // show schedule date if is schedule news.
          $date = $dateSchedule ? $dateSchedule : strtotime($news['date']);
          echo Util::displayDate($date);
          ?>
        </div>
        <a href="<?php echo $news['link'];?>" class="btn-arrow">
          <i class="fas fa-arrow-right"></i>
        </a>
      </div>
    </div>
  </div>
</div>
