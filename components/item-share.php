<div class="panel sidebar-social">
  <div class="row">
    <div class="col-12 col-xl">
      <h4>
        <?php _e('COMPARTILHE', 'morrinhos'); ?>
      </h4>
    </div>
    <div class="col-12 col-xl mt-2 mt-xl-0 share-buttons">
      <a href="http://facebook.com/share.php?u=<?php the_permalink() ?>&amp;t=<?php echo urlencode(get_the_title('','', false)) ?>" target="_blank" title="Compartilhar <?php the_title();?> no Facebook" class="mr-1">
        <img class="icon" src="<?php echo images_path() . '/compartilhar_facebook_1.svg'; ?>">
      </a>
      <a href="http://twitter.com/intent/tweet?text=<?php echo urlencode(get_the_title());?>&url=<?php the_permalink();?>" title="Twittar sobre <?php the_title();?>" target="_blank" class="mr-1">
        <img class="icon" src="<?php echo images_path() . '/compartilhar_twitter_1.svg'; ?>">
      </a>
      <a href="whatsapp://send?text=<?php the_permalink() ?>" data-action="share/whatsapp/share">
        <img class="icon" src="<?php echo images_path() . '/compartilhar_whatsapp_1.svg'; ?>">
      </a>
    </div>
  </div>
</div>