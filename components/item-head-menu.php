<div class="colors-bar">
  <div class="color-bar1"></div>
  <div class="color-bar2"></div>
  <div class="color-bar3"></div>
  <div class="color-bar4"></div>
  <div class="color-bar5"></div>
  <div class="color-bar6"></div>
</div>


<nav class="navbar navbar-dark navbar-expand-lg navbar-floating px-xl-5 px-md-0">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="mobile-navbar-brand" href="/">
  </a>    
  <div class="collapse navbar-collapse h-100" id="navbarToggler">
    <div class="col-sm-3 h-100">
      <a class="navbar-brand h-100" href="/">
      </a>
    </div>
    <div class="col-sm-9 h-100">
      <ul class="navbar-nav h-100">
        <?php custom_menu_head('head-menu'); ?>
      </ul>
    </div>
    <div class="text-right social-media-top">
      <div class="font-size-buttons col" aria-labelledby="fontSize">
        <button class="btn reduce">
          A-
        </button>
        |
        <button class="btn increase">
        </button>
          A+
      </div>

  </div>
</nav>
<?php get_template_part('components/item', 'searchform'); ?>
