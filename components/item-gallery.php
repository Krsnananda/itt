<?php
  $galleryImg = Util::getWidgetImgPath('gallery-img');
  $galleryText = Util::getFirstWidgetObj('gallery-text');
  $gallery = Util::getWidgetImgPath('gallery');
?>

<div class="gallery py-4">
  <div class="container gallery-text">
    <img src="<?php echo $galleryImg[0]; ?>">
    <p class="mt-2"><?php echo $galleryText['content']; ?></p>
  </div>      
  <?php if (count($gallery) > 0): ?>
    <div class="scrolling-wrapper">
      <?php foreach ($gallery as $key => $img):?>
      <div  class="gallery-image background-image" style="background-image: url('<?php echo $img; ?>')">
      </div>
      <?php endforeach; ?>
    </div>
  <?php endif ?>
</div>

