<?php
/*------------------------------------*\
    Includes / Requires
\*------------------------------------*/
require_once(get_template_directory().'/model/index.php');


/*------------------------------------*\
    Functions
\*------------------------------------*/

// Paginação 
function pagination()
{
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages
    ));
}

function images_path() {
    return get_template_directory_uri(). '/public/assets/images';
}

/*
// filtragens de custom post types para pesquisa
function cpt_search( $query ) {

    if ( is_search() && $query->is_main_query() && $query->get( 's' ) ){

        $query->set( 'post_type', array(
            'page', 
            'post',
            'news',
            'company',
            'history',
            'infrastructure',
            'program'  
        ) );
    }
}
add_filter('pre_get_posts', 'cpt_search');

function get_all_custom_post_types() {
    $dirs = scandir(get_template_directory().'/model/post-type/');
    unset($dirs[0]);
    unset($dirs[1]);
    return $dirs;
}

// Remove posts_per_page limit from archive lists
function wpsites_query_archive( $query ) {
    if ( $query->is_archive() && $query->is_main_query() && !is_admin() ) {
        $query->set( 'posts_per_page', -1 );
    }
}
add_action( 'pre_get_posts', 'wpsites_query_archive' );

 */

// Localisation Support
load_theme_textdomain('morrinhos', get_template_directory() . '/languages');

/*------------------------------------*\
    Actions + Filters + ShortCodes
\*------------------------------------*/
//Add Actions

// Add Theme suports
add_theme_support('post-thumbnails'); // Adiciona a imagem de descricao no post
add_theme_support('menus');
add_image_size('large', 700, '', true); 
add_image_size('medium', 250, '', true); 
add_image_size('small', 120, '', true); 
add_image_size('custom-size', 700, 200, true);

// disable admin bar for all users
show_admin_bar(false);
