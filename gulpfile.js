
const { series, parallel, src, dest, watch } = require('gulp'),
    babel = require('gulp-babel'),
    minify = require('gulp-minify'),
    sass   = require('gulp-sass'),
    concat = require('gulp-concat'),
    gettext= require('gulp-gettext'),
    notify = require('gulp-notify'),
    gutil  = require('gulp-util'),
    cleanCSS = require('gulp-clean-css');

function buildCss() {
  return src('scss/**/*.scss')
    .pipe(concat('main.css'))
    .pipe(sass())
    .on('error', function(err) {
      notify().write(err);
      this.emit('end');
    })
    .pipe(gutil.env.type === 'production' ? cleanCSS({compatibility: 'ie8'}) : gutil.noop())
    .pipe(dest('public/assets/stylesheets'));
};

function buildJs() {
  return src('js/**/*.js')
    .pipe(concat('bundle.js'))
    .pipe(babel())
    .on('error', function(err) {
      notify().write(err);
      this.emit('end');
    })
    //only uglify if gulp is ran with '--type production'
    //.pipe(gutil.env.type === 'production' ? uglify() : gutil.noop()) 
    .pipe(gutil.env.type === 'production' ? minify({
      ext:{
        src:'-normal.js',
        min:'.js'
      },
    }) : gutil.noop())
    .pipe(dest('public/assets/javascript'));    
};

function buildLanguage() {
  return src('languages/*.po')
    .pipe(gettext())
    .pipe(dest('languages'));
}

function watchFiles() {
  watch('js/**/*.js', buildJs);
  watch('scss/**/*.scss', buildCss);
}

exports.default = series(buildCss, buildJs, watchFiles);

exports.build = series(buildCss, buildJs, buildLanguage);
