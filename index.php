<?php get_header(); ?>

<header class="header clear">  
  <?php get_template_part('components/item', 'head-menu'); ?>
  <?php get_template_part('components/item', 'carousel'); ?>
</header>
<main role="main" class="home">
  <div class="container my-4">
    <div class="row">
      <div class="col-md-8 mt-3">
        <?php get_template_part('components/item', 'home-list-news'); ?>
      </div>
      <div class="col-md-4 ">
        <?php get_template_part('components/item', 'sidebar'); ?>
        <?php get_template_part('components/item', 'schedule'); ?>
        <?php get_template_part('components/item', 'placar'); ?>
      </div>
    </div>
  </div>

</main>

<?php get_footer(); ?>