'use strict'; // jQuery function to check whether elements are visible

$.fn.visible = function (partial) {
  var $t = $(this),
      $w = $(window),
      viewTop = $w.scrollTop(),
      viewBottom = viewTop + $w.height(),
      _top = $t.offset().top,
      _bottom = _top + $t.height(),
      compareTop = partial === true ? _bottom : _top,
      compareBottom = partial === true ? _top : _bottom;

  return compareBottom <= viewBottom && compareTop >= viewTop;
}; // utils functions


var getUrlParameter = function getUrlParameter(sParam) {
  var sPageURL = window.location.search.substring(1),
      sURLVariables = sPageURL.split('&'),
      sParameterName,
      i;

  for (i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split('=');

    if (sParameterName[0] === sParam) {
      return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
    }
  }
};

(function () {
  'use strict';

  $('.card:not(.not-click)').click(function () {
    var link = $(this).find('a').attr('href') || $(this).data('href');

    if (link) {
      if ($(this).data('new-tab')) {
        var win = window.open(link, '_blank');
        win.focus();
      } else {
        window.location.href = link;
      }
    }
  });
})();

(function () {
  'use strict';

  var fontSize;
  var fontDiff = 2;
  var minFontSize = 14;
  var maxFontSize = 20;
  var currentFontSize = localStorage.getItem('fontSize');

  var setFontSize = function (size) {
    document.getElementsByTagName('html')[0].style.fontSize = size + 'px';
  };

  var changeFontSize = function (operator) {
    fontSize = parseInt(localStorage.getItem('fontSize')) + fontDiff * operator;

    if (fontSize <= maxFontSize && fontSize >= minFontSize) {
      localStorage.setItem('fontSize', fontSize);
      setFontSize(fontSize);
    }
  };

  if (currentFontSize) {
    setFontSize(currentFontSize);
  } else {
    localStorage.setItem('fontSize', 16);
  }

  $('.font-size-buttons .reduce').click(function () {
    changeFontSize(-1);
  });
  $('.font-size-buttons .increase').click(function () {
    changeFontSize(1);
  });
})(); // media query function for min-width: 992px (lg)


function minWidth992(x) {
  if (x.matches) {
    // If media query matches
    // make parent menu clickable link
    $('.navbar .dropdown > a').mousedown(function (e) {
      if (e.which == 1) {
        location.href = this.href;
      }
    });
  } else {}
} // Redirect when there is no child item


$('.navbar .dropdown > a').click(function (e) {
  if ($(this).closest('li').find('.dropdown-menu a').length == 0 && e.which == 1) {
    location.href = this.href;
  }
});
var mediaQuery992 = window.matchMedia("(min-width: 992px)");
minWidth992(mediaQuery992);
mediaQuery992.addListener(minWidth992);