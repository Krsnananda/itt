# Site Morrinhos do Sul
Template para Wordpress

## Install
* Baixar wordpress
* Instalar
* cd wp-content/themes/
* git clone ...
* cd morrinhos
* npm install
* mkdir wp-content/uploads
* chmod 777 wp-content/uploads
* [Plugin] https://wordpress.org/plugins/advanced-custom-fields/
* [Plugin] https://wordpress.org/plugins/contact-form-7/
* [Plugin] https://wordpress.org/plugins/contact-form-cfdb7/
* reCaptcha
    * eagle.tecnologia.design@gmail.com
    * Menu do admin, Contato>Integração - adicionar chaves

## Conteúdo gerenciável
* Appearence > Customize > Site Identity: trocar descrição e favicon
* Menus
* Widgets
* Pages
* Custom Fields: Notícias...
* Contact Forms
* Mailchimp: Chave de API

## Importante - ao criar um novo post type
* Ao criar um novo custom post type, para acessar ele na url ex localhost/pessoa/
é necessário abrir o wp-admin, ir no menu permalinks(/wp-admin/options-permalink.php) e alterar e salvar para que 
a url passe a funcionar.
Ver sobre flush_rewrite_rules()

## Comandos

* Monitora modificações e compila css e js
    * npm start
* Compilar css e js e language
    * npm run build

## Deploy para produção
* Alterar versões de js/css em model/base/enqueue.php
* Acessar via ssh
* Entrar na pasta do tema
* git pull origin master
* npm run buildprod
