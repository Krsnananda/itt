<?php get_header(); ?>
<header class="header clear">  
  <?php get_template_part('components/item', 'head-menu'); ?>
</header>
<main role="main">
  <div class="container mt-4">
    <div class="row">
      <div class="col-md-8 mt-3">
        <?php
            get_template_part('lists/list-search');            
        ?>
        <?php get_template_part('components/item','pagination'); ?>
      </div>
    </div>
  </div>
</main>

<?php get_footer(); ?>
