<?php

if (function_exists('acf_add_local_field_group')) {

  acf_add_local_field_group(array(
    'key' => 'group_23',
    'title' => 'Placar',
    'fields' => array (
      array(
        'key' => 'placar-time1',
        'label' => 'Time 1',
        'name' => 'time1',
        'type' => 'text',
        'maxlength' => 200,
        'placeholder' => ''
      ),
      array(
        'key' => 'placar-pontos1',
        'label' => 'Time 1 - pontuação',
        'name' => 'pontos1',
        'type' => 'number',
        'maxlength' => 2,
        'placeholder' => ''
      ),
      array(
        'key' => 'placar-time2',
        'label' => 'Time 2',
        'name' => 'time2',
        'type' => 'text',
        'maxlength' => 200,
        'placeholder' => ''
      ),
      array(
        'key' => 'placar-pontos2',
        'label' => 'Time 2 - pontuação',
        'name' => 'pontos2',
        'type' => 'number',
        'maxlength' => 2,
        'placeholder' => ''
      ),
      array(
        'key' => 'placar-date',
        'label' => 'Data',
        'name' => 'date',
        'type' => 'date_time_picker'
      ),
    ),

    'location' => array (
      array (
        array (
          'param' => 'post_type',
          'operator' => '==',
          'value' => 'placar',
        ),
      ),
    ),
    'style' => 'seamless',
  ));

}
