<?php

if (function_exists('acf_add_local_field_group')) {

  acf_add_local_field_group(array(
    'key' => 'group_2',
    'title' => 'Notícia',
    'fields' => array (
      array(
        'key' => 'news-description',
        'label' => 'Descrição',
        'name' => 'description',
        'type' => 'textarea',
        'maxlength' => 200,
        'placeholder' => ''
      ),
      array(
        'key' => 'news-font-source',
        'label' => 'Fonte da imagem',
        'name' => 'source',
        'type' => 'text',
        'maxlength' => 200,
        'placeholder' => 'link da fonte da imagem'
      ),
      array(
        'key' => 'news-date',
        'label' => 'Data',
        'name' => 'date-schedule',
        'instructions' => 'Caso deseje incluir esta notícia na Agenda, preencha este campo e marque a categoria agenda/schedule.',
        'type' => 'date_time_picker'
      ),
    ),

    'location' => array (
      array (
        array (
          'param' => 'post_type',
          'operator' => '==',
          'value' => 'news',
        ),
      ),
    ),
    'style' => 'seamless',
  ));

}
