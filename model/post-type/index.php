<?php
// news
require_once(get_template_directory().'/model/post-type/news/post.php');
require_once(get_template_directory().'/model/post-type/news/field.php');
require_once(get_template_directory().'/model/post-type/news/functions.php');

// placar
require_once(get_template_directory().'/model/post-type/placar/post.php');
require_once(get_template_directory().'/model/post-type/placar/field.php');
require_once(get_template_directory().'/model/post-type/placar/functions.php');
