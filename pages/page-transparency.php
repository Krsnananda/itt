<?php
/* Template name: Portal Transparência */
?>
<?php get_header(); ?>
<header class="header clear">
  <?php
    set_query_var('title', 'PORTAL TRANSPARÊNCIA');
    get_template_part('components/item', 'head-menu');
  ?>
</header>
<main role="main">
  <?php get_template_part('components/item', 'head-page'); ?>
  <div class="container my-4">
    <div class="row">
      <div class="col-md-8">
      <?php if (have_posts()): while (have_posts()) : the_post(); ?>
        <article class="panel" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
          <h3 class="text-center my-3">
            <?php echo get_the_title(); ?>
          </h3>
          <?php the_content(); ?>
          <?php edit_post_link("Editar"); ?>
        </article>
      <?php endwhile; ?>
      <?php endif; ?>
      </div>
      <div class="col-sm-4 mt-3">
        <div class="mb-3">
          <?php get_template_part('components/item', 'share'); ?>
        </div>
        <?php custom_menu_sidebar('portal-transparecncia-menu'); ?> 
      </div>
    </div>
  </section>
</main>
<?php get_footer(); ?>