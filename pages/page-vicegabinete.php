<?php
/* Template name: Gabinete do Vice-Prefeito */
?>
<?php get_header(); ?>
<header class="header clear">
  <?php 
    set_query_var('title', 'GABINETE DO VICE-PREFEITO');
    get_template_part('components/item', 'head-menu');
  ?>
</header>
<main role="main">
  <?php get_template_part('components/item', 'head-page'); ?>
  <div class="container my-4 gabinete">
    <div class="row">
      <div class="col-md-8">
      <?php if (have_posts()): while (have_posts()) : the_post(); ?>
        <div class="panel mb-3">
          <div class="row">
            <div class="col-md-3 text-center">
              <img src="<?php echo get_the_post_thumbnail_url(); ?>">
            </div>
            <div class="col-md-9 description">
              <h3 class="my-0">
                <?php echo get_the_title(); ?>
              </h3>
              VICE-PREFEITO
            </div>
          </div>
        </div>
        <article class="panel" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
          <?php the_content(); ?>
          <?php edit_post_link("Editar"); ?>
        </article>
      <?php endwhile; ?>
      <?php endif; ?>
      </div>
      <div class="col-sm-4 mt-3">
        <div class="mb-3">
          <?php get_template_part('components/item', 'share'); ?>
        </div>
        <?php custom_menu_sidebar('sidebar'); ?> 
      </div>
    </div>
  </section>
</main>
<?php get_footer(); ?>