<?php get_header(); ?>
<header class="header clear">
  <?php get_template_part('components/item', 'head-menu'); ?>
</header>
<main role="main">
  <?php get_template_part('components/item', 'head-page'); ?>
  <div class="container my-4">
    <div class="row">
      <div class="col-md-8">
      <?php if (have_posts()): while (have_posts()) : the_post(); ?>
        <article class="panel" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
          <?php the_content(); ?>
          <?php edit_post_link("Editar"); ?>
        </article>
      <?php endwhile; ?>
      <?php endif; ?>
      </div>
      <div class="col-sm-4 mt-3">
        <?php get_template_part('components/item', 'share'); ?>
      </div>
    </div>
  </section>
</main>
<?php get_footer(); ?>