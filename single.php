<?php
  if (get_post_type() == 'placar') {
    header('Location: /placar');
  }
  
  get_header(); 
?>
<header class="header clear">
  <?php get_template_part('components/item', 'head-menu'); ?>
</header>
<main role="main">
  <?php get_template_part('components/item', 'head-page'); ?>
  <div class="container mt-4">
    <div class="row d-flex flex-wrap">
      <div class="col-sm-8">
        <?php 
        $lists = array('news');
        if (in_array(get_post_type(), $lists)) {
            get_template_part('singles/single-' . get_post_type());
        } else {
            get_template_part('singles/single-default');
        }
        ?>

      </div>
      <div class="col-sm-4 mt-3">
        <?php get_template_part('components/item', 'sidebar'); ?>
      </div>
    </div>
  </div>
</main>

</main>
<?php get_footer(); ?>
