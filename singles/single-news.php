<?php
  
  $id = $post->ID;
  $terms_full = get_the_terms($post->ID, 'category_news');
  if (!$terms_full) {
    
    $terms = null;

  } else {

    foreach ($terms_full as $row) {
      $terms[] = $row->name;
    }
  }
?>
<section class="single-page pt-4">
  <?php if (have_posts()): while (have_posts()) : the_post(); ?>
    <div class="panel">
      <?php if (get_field('date-schedule')): ?>
        <div class="w-100 h-100 d-flex">
            <div class="schedule-date">
              <?php
              $dateSchedule = strtotime(Util::datePtToEn(get_field('date-schedule')));
              echo date('d/m', $dateSchedule);
              ?>
            </div>
        </div>
      <?php endif; ?>
      <?php if (get_field('description')): ?>
        <h5>
          <?php echo get_field('description'); ?>
        </h5>
      <?php endif; ?>
      <div class="row">
        <!-- Conteudo -->  
        <div class="col">
          <img src="<?php echo get_the_post_thumbnail_url(); ?>" 
-            alt="<?php echo get_post_meta( $thumbnail_id, '_wp_attachment_image_alt', true); ?>"
-            class="mb-2">
          <div class="small">
            <?php echo get_field('source'); ?>
          </div>
          <div class="mt-3 mb-5">
            <?php the_content(); ?>
            <?php edit_post_link("Editar"); ?>
          </div>
          <hr>
          <div class="row">
            <div class="small col-md-4">
              <?php echo Util::displayDate(strtotime(get_the_date('Y-m-d'))); ?>
            </div>
            <?php if($terms != null): ?>
            <div class="col-md-8 text-right">
              <?php foreach ($terms as $row): ?>
                <span class="badge text-right"><?php _e($row, 'morrinhos'); ?></span>
              <?php endforeach; ?>
            </div>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  <?php endwhile; ?>
    <!-- Relacionados -->
    <div class="container">
      <div class="row">
      <div class="col-md-8 offset-md-1 mt-5">
        <?php //get_template_part('sections/item','news-related'); ?> 
      </div>
      </div>
    </div>
  <?php else: ?>
      <h1><?php _e( 'Não encontrado.', 'html5blank' ); ?></h1>
  <?php endif; ?>
</section>
<?php //get_template_part('components/item', 'modal-lightbox'); ?>