<?php
  
$query_args = array(
  'post_type' => 'placar',
  'post_status' => 'publish',
  'orderby' => 'date',
  'order'   => 'ASC',
  'posts_per_page' => -1
);
$the_query = new WP_Query($query_args);
$placares = array();

while ($the_query->have_posts()){
  $the_query->the_post();
  $placares[] = array(
    'title' => get_the_title(),
    'time1' => get_field('time1'),
    'pontos1' => get_field('pontos1'),
    'time2' => get_field('time2'),
    'pontos2' => get_field('pontos2'),
    'date' => get_field('date'),
    'link' => get_post_permalink()
  );
}
function sortByDate($a, $b) {
  return strtotime(Util::datePtToEn($b['date'])) - strtotime(Util::datePtToEn($a['date']));
}
usort($placares, 'sortByDate');

?>
<?php 
  set_query_var('title', __('CAMPEONATO MUNICIPAL', 'morrinhos'));
  get_template_part('components/item', 'head-page');
?>
<?php if ($the_query->have_posts()) : ?>
 <div class="container mt-4">
    <div class="row d-flex flex-wrap">
      <div class="col-sm-8">
        <div class="panel placar">
          <?php 
            set_query_var('placares', $placares);
            get_template_part('components/item', 'panel-placar');
          ?>
        </div>
      </div>
      <div class="col-sm-4 mt-3">
        <?php get_template_part('components/item', 'sidebar'); ?>
      </div>
    </div>
  </div>
<?php endif ?>