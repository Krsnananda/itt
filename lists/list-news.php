<?php

  $is_schedule = get_query_var('term') == 'schedule';
  
  $showAll = $_GET['showAll'];
  $posts_per_page = 8;

  $query_args = array(
    'post_type' => 'news',
    'post_status' => 'publish',
    'posts_per_page' => $showAll ? -1 : $posts_per_page
  );

  if (get_query_var('term')) {
    $query_args['tax_query'] = array(
        array(
          'taxonomy' => 'category_news',
          'field'    => 'slug',
          'terms'    => get_query_var('term'),
        ),
      );
  } else {
    $query_args['tax_query'] = array(
        array(
          'taxonomy' => 'category_news',
          'field'    => 'slug',
          'terms'    => 'schedule',
          'operator' => 'NOT IN'
        ),
      );
  }

  $the_query = new WP_Query($query_args);
  $news = array();

  while ($the_query->have_posts()){
    $the_query->the_post();
    $news[] = array(
      'id' => $post->ID,
      'title' => get_the_title(),
      'description' => get_field('description'),
      'date-schedule' => get_field('date-schedule'),
      'date' => get_the_date('Y-m-d'),
      'link' => get_post_permalink(),
      'img' => get_the_post_thumbnail_url(),
    );
  }

  if ($is_schedule) {
    function sortByDate($a, $b) {
      return strtotime(Util::datePtToEn($b['date-schedule'])) - strtotime(Util::datePtToEn($a['date-schedule']));
    }
    usort($news, 'sortByDate');
  }

?>
<?php 
  set_query_var('title', $is_schedule ? __('Agenda', 'morrinhos') : __('Notícias', 'morrinhos'));
  get_template_part('components/item', 'head-page');
?>
<section class="list">
  <div class="container mt-4">
    <div class="row d-flex flex-wrap">
      <div class="col-sm-8 m-0 p-0 order-md-1 order-2">
        <?php if (count($news) > 0): foreach ($news as $key => $row):?>
          <?php if ($key == $posts_per_page): ?>
            <div id="showmore"></div>
          <?php endif;?>
          <div class="col-sm-12 mb-4">
            <?php 
              set_query_var('news',$row);
              get_template_part('components/item','card-news');
            ?>
          </div> 
          <?php endforeach; ?>
          <?php if (!$showAll && count($news) >= $posts_per_page): ?>
            <div class="col-sm-12 mb-4">
              <a class="btn btn-show-more w-100" href="?showAll=true#showmore">
                <?php _e('VEJA MAIS', 'morrinhos'); ?>
              </a>
            </div>
          <?php endif ?>
        <?php else: ?>
          <div class="col-sm-12 mb-2">
            <div class="card card-horizontal not-click">
              <div class="card-body">
                <div class="row">
                  <div class="col-md-10">
                    <div class="row">
                      <div class="col-md-12 d-flex">
                      </div>
                      <div class="col-md-10">
                        <?php echo _e('Nenhum resultado encontrado', 'morrinhos'); ?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div> 
        <?php endif ?>
      </div>
      <div class="col-sm-4 order-md-2 order-1">
        <div class="panel categories my-4 not-click card-sticky">
          <div class="">
            <h5>
              <?php echo  _e('CATEGORIAS', 'morrinhos'); ?>
            </h5>
          </div>
          <div class="card-body">
            <?php 
              $taxonomy = 'category_news';
              // busca todas as taxonomias e armazena em um array
              $taxonomies = get_taxonomies('','names');

              // se existir a taxonomia ele apresenta os termos para filtrar
              if (in_array($taxonomy, $taxonomies)) {

                $terms = get_terms($taxonomy);

                foreach ($terms as $linha) {
                  $taxonomy_url = '?' . $taxonomy . '=';
                  $terms_url = $taxonomy_url . $linha->slug;
                  if (get_query_var('term') == $linha->slug) {
                    $class = 'secondary';
                  } else {
                    $class = 'primary';
                  }
                  
                  echo '<a href="' . $terms_url . '" >' . 
                        '<span class="badge badge-'.$class.'">'.
                          $linha->name .
                        '</span>'.
                      '</a>';
                }
              }
            ?>
          </div>
        </div>        
      </div>
    </div>
  </div>
</section>
