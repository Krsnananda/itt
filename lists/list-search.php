<?php
  $post_types = array(
    'news' => 'Notícia',
    'page' => 'Página',
    'placar' => 'Placar',
  );
?>
<section>
  <?php if (have_posts()): while (have_posts()) : the_post(); ?>
  <div class="col-sm-12 mb-2">
    <div class="card card-horizontal">
      <div class="card-body">
        <div class="row">
          <div class="col-md-2 text-md-center my-auto">
                <span class="badge badge-purple">
                  <?php 
                    echo $post_types[get_post_type()];
                  ?>
                </span>
          </div>
          <div class="col-md-10">
            <div class="row">
              <div class="col-md-12">
                <h4 class="card-title"><?php the_title(); ?></h4>
              </div>
              <div class="col-md-10">
                <?php echo get_the_date() ;?>
              </div>
              <div class="col-md-2">
                <a href="<?php echo  get_the_permalink();?>" class="btn-arrow">
                  <i class="fas fa-arrow-right"></i>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div> 
  <?php endwhile; ?>
  <?php else: ?>
      <div class="col-sm-12 mb-2">
        <div class="card card-horizontal not-click">
          <div class="card-body">
            <div class="row">
              <div class="col-md-10">
                <div class="row">
                  <div class="col-md-12 d-flex">
                    <h4 class="card-title"><?php echo _e('TERMO BUSCADO', 'morrinhos'); ?>:</h4>
                    <p class="ml-2"><?php echo get_search_query(); ?></p>
                  </div>
                  <div class="col-md-10">
                    <?php echo _e('Nenhum resultado encontrado', 'morrinhos'); ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> 
  <?php endif; ?>
  </section>