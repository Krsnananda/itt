// media query function for min-width: 992px (lg)
function minWidth992(x) {
    if (x.matches) { // If media query matches
      // make parent menu clickable link
      $('.navbar .dropdown > a').mousedown(function(e) {
        if (e.which == 1) {
          location.href = this.href;
        }
      });
    } else {
    }
}
// Redirect when there is no child item
$('.navbar .dropdown > a').click(function(e) {
  if ($(this).closest('li').find('.dropdown-menu a').length == 0 && e.which == 1) {
    location.href = this.href;
  }
});

var mediaQuery992 = window.matchMedia("(min-width: 992px)");
minWidth992(mediaQuery992);
mediaQuery992.addListener(minWidth992);
