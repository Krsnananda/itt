(function () {

    'use strict';

    $('.card:not(.not-click)').click(function() {
        var link = $(this).find('a').attr('href') || $(this).data('href');
        if (link) {
            if ($(this).data('new-tab')) {
                var win = window.open(link, '_blank');
                win.focus();
            } else {
                window.location.href = link;
            }
        }
    });

})();