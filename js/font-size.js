(function () {

    'use strict';
    
    var fontSize;
    var fontDiff = 2;
    var minFontSize = 14;
    var maxFontSize = 20;
    var currentFontSize = localStorage.getItem('fontSize');

    var setFontSize = function (size) {
        document.getElementsByTagName('html')[0].style.fontSize = size + 'px';
    }

    var changeFontSize = function (operator) {
        fontSize = parseInt(localStorage.getItem('fontSize')) + (fontDiff * operator);
        if (fontSize <= maxFontSize && fontSize >= minFontSize) {
            localStorage.setItem('fontSize', fontSize);
            setFontSize(fontSize);    
        }
    }

    if (currentFontSize) {
        setFontSize(currentFontSize);
    } else {
        localStorage.setItem('fontSize', 16);
    }

    $('.font-size-buttons .reduce').click(function() {
        changeFontSize(-1);
    });
    $('.font-size-buttons .increase').click(function() {
        changeFontSize(1);
    });


})();